# http://math.unipa.it/~grim/SiSala1.pdf
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
import scipy.ndimage
def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


def diamondSquare(array, sigma= 1):

	w,h = array.shape
	# Square step
	newSigma = sigma/2
	array[(w-1)/2,(h-1)/2] = ((array[0,0]+ array[0,h-1] + array[w-1,0] + array[w-1,h-1])/4) + np.random.normal(0,newSigma)
	# Diamond step :
	array[0,(h-1)/2] = 		((array[0,0]+ array[0,h-1] + array[(w-1)/2,(h-1)/2] )/3) + np.random.normal(0,newSigma)
	array[(w-1)/2,0] =  	((array[0,0]+ array[w-1,0] + array[(w-1)/2,(h-1)/2] )/3) + np.random.normal(0,newSigma)
	array[(w-1),(h-1)/2] =  ((array[w-1,0]+ array[w-1,h-1] + array[(w-1)/2,(h-1)/2] )/3) + np.random.normal(0,newSigma)
	array[(w-1)/2,(h-1)] =  ((array[0,h-1]+ array[w-1,h-1] + array[(w-1)/2,(h-1)/2] )/3) + np.random.normal(0,newSigma)
	
	if(w>=3 and h>=3):
		array[0:(w+1)/2,0:(h+1)/2] = diamondSquare(array[0:(w+1)/2,0:(h+1)/2], newSigma)
		array[(w-1)/2:w,0:(h+1)/2] = diamondSquare(array[(w-1)/2:w,0:(h+1)/2], newSigma)
		array[0:(w+1)/2,(h-1)/2:h] = diamondSquare(array[0:(w+1)/2,(h-1)/2:h], newSigma)
		array[(w-1)/2:w,(h-1)/2:h] = diamondSquare(array[(w-1)/2:w,(h-1)/2:h], newSigma)
		
		array[(w+1)/4:3*(w+1)/4,(h+1)/4:3*(h+1)/4] = diamondSquare(array[(w+1)/4:3*(w+1)/4,(h+1)/4:3*(h+1)/4], newSigma)

	return array

# Choisir des dimensions impaires
w = 65
h = 65
# Data to plot.
x, y = np.meshgrid(np.arange(h), np.arange(w))

z = np.zeros((w,h), dtype = np.float64)

initVal = 1
z[0,0] = 0.3
z[0,h-1] = 0.3
z[w-1,0] = 0.35
z[w-1,h-1] = 0.2
z = diamondSquare(z)
z = scipy.ndimage.filters.gaussian_filter(z, 1.0)
fig = plt.figure(figsize=plt.figaspect(2.))
ax = fig.add_subplot(2, 1, 1)
cs = ax.contour(x, y, z, 30,cmap='binary')

ax.contour(cs, colors='k')

# Second plot
ax = fig.add_subplot(2, 1, 2, projection='3d')

# surf = ax.plot_surface(x,y,z, cmap= "binary", linewidth=0, antialiased=False)
ax.contour3D(x,y,z , 50, cmap='binary')
plt.show()