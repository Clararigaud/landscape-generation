from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
import scipy.ndimage
def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

w = 60
h  =80
# Data to plot.
x, y = np.meshgrid(np.arange(w), np.arange(h))
# z = np.sin(0.5 * x) * np.cos(0.52 * y)
a = gaussian(np.linspace(0, h, h), h/2, h/2)
b = gaussian(np.linspace(0, w, w), w/2, w/2	)
# z = np.multiply(np.random.rand(h,w), 1000*np.dot(a[:,None] ,b[None,:]))
# z = np.cos(z);


# z = np.multiply(np.random.rand(h,w), np.cos(np.dot(a[:,None] ,b[None,:])))

z = np.random.rand(h,w)/1000;
z = scipy.ndimage.filters.gaussian_filter(z, 4.0)
fig = plt.figure(figsize=plt.figaspect(2.))
ax = fig.add_subplot(2, 1, 1)
cs = ax.contour(x, y, z, 30)

ax.contour(cs, colors='k')

# Second plot
ax = fig.add_subplot(2, 1, 2, projection='3d')

# surf = ax.plot_surface(x,y,z, cmap= "binary", linewidth=0, antialiased=False)
ax.contour3D(x,y,z , 50, cmap='binary')
plt.show()